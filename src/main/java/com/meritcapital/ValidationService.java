package com.meritcapital;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@Service
public class ValidationService {
    private static String AMERICAN_STYLE = "AMERICAN";
    private static String EUROPEAN_STYLE = "EUROPEAN";
    private static List<String> SUPPORTED_CUSTOMERS = Arrays.asList("PLUTO1", "PLUTO2");
    private static List<String> SUPPORTED_STYLES = Arrays.asList(AMERICAN_STYLE, EUROPEAN_STYLE);
    private static String SUPPORTED_LEGAL_ENTITY = "CS[ _]?Zurich";
    private static String CURRENCY = "/^AED|AFN|ALL|AMD|ANG|AOA|ARS|AUD|AWG|AZN|BAM|BBD|BDT|BGN|BHD|BIF|BMD|BND|BOB|BRL|BSD|BTN|BWP|BYR|BZD|CAD|CDF|CHF|CLP|CNY|COP|CRC|CUC|CUP|CVE|CZK|DJF|DKK|DOP|DZD|EGP|ERN|ETB|EUR|FJD|FKP|GBP|GEL|GGP|GHS|GIP|GMD|GNF|GTQ|GYD|HKD|HNL|HRK|HTG|HUF|IDR|ILS|IMP|INR|IQD|IRR|ISK|JEP|JMD|JOD|JPY|KES|KGS|KHR|KMF|KPW|KRW|KWD|KYD|KZT|LAK|LBP|LKR|LRD|LSL|LYD|MAD|MDL|MGA|MKD|MMK|MNT|MOP|MRO|MUR|MVR|MWK|MXN|MYR|MZN|NAD|NGN|NIO|NOK|NPR|NZD|OMR|PAB|PEN|PGK|PHP|PKR|PLN|PYG|QAR|RON|RSD|RUB|RWF|SAR|SBD|SCR|SDG|SEK|SGD|SHP|SLL|SOS|SPL|SRD|STD|SVC|SYP|SZL|THB|TJS|TMT|TND|TOP|TRY|TTD|TVD|TWD|TZS|UAH|UGX|USD|UYU|UZS|VEF|VND|VUV|WST|XAF|XCD|XDR|XOF|XPF|YER|ZAR|ZMW|ZWD$/";
    private static String CURRENT_DATE = "18.07.2017"; //не понятна как связана логика проверки trade с текущей датой. Она на год позже всех торгов.
    /**
     * Validate trades by specific params
     * Generate error information
     *
     * @param trade - OPTION, SPOT, FORWARD
     */
    void validateCommonTradeConditions(Trade trade) {
        String errorText = "";
        Pattern lePattern = Pattern.compile(SUPPORTED_LEGAL_ENTITY);

        if (!SUPPORTED_CUSTOMERS.contains(trade.getCustomer())) {
            errorText += "Unsupported customer " + trade.getCustomer() + "; ";
        }

        if (!lePattern.matcher(trade.getLegalEntity()).matches()) {
            errorText += "Unsupported legal entity " + trade.getLegalEntity() + "; ";
        }

        errorText += checkCurrencyISO(trade);

        // не понятна логика по проверке опционов, они проверяются по условию
        // value date cannot be before trade date или нет т.к. в json отсутсвует значение valueDate
        if (trade.getType().contains("Option")) {
            errorText += checkOptions(trade);
        }

        // не понятна полностью логика, valueDate && tradeDate обязательны ли для заполнения всех trade'ов или нет
        if (trade.getValueDate() == null || trade.getTradeDate() == null) {
            errorText += "One of the date values is null; ";
        } else if (trade.getValueDate().before(trade.getTradeDate())) {
            errorText += "Value date cannot be before trade date; ";
        } else {
            errorText += checkWeekend(trade.getValueDate());
            errorText += checkDatesByTypes(trade);
        }

        if (!errorText.isEmpty())
            System.out.println(errorText);
        trade.setErrorText(errorText);
    }

    /**
     * Validate currency ISO code
     *
     * @param trade - OPTION, SPOT, FORWARD
     * @return errorText
     */
    private String checkCurrencyISO(Trade trade) {
        String errorText = "";
        Pattern curPattern = Pattern.compile(CURRENCY);
        if (trade.getCcyPair() != null) {
            if (!curPattern.matcher(trade.getCcyPair().substring(0, 3)).matches() ||
                    !curPattern.matcher(trade.getCcyPair().substring(3, 6)).matches()) {
                errorText += "Invalid currency pair ISO code " + trade.getCcyPair() + "; ";
            }
        }
        if (trade.getPayCcy() != null && !curPattern.matcher(trade.getPayCcy()).matches()) {
            errorText += "Invalid currency pay ISO code " + trade.getPayCcy() + "; ";
        }
        if (trade.getPremiumCcy() != null && !curPattern.matcher(trade.getPremiumCcy()).matches()) {
            errorText += "Invalid premium currency ISO code " + trade.getPremiumCcy() + "; ";
        }
        return errorText;
    }

    /**
     * Validate date values depending of trade type
     *
     * @param trade - OPTION, SPOT, FORWARD
     * @return error text
     */
    private String checkDatesByTypes(Trade trade) {
        long diff = trade.getValueDate().getTime() - trade.getTradeDate().getTime();
        long tradeDurationInDays = diff / (1000 * 60 * 60 * 24);

        switch (trade.getType()) {
            case "Forward": {
                if (tradeDurationInDays > 5) {
                    return "Forward trade duration more then 5 days; ";
                }
            }
            case "Spot": {
                if (tradeDurationInDays > 2) {
                    return "Spot trade duration more then 2 days; ";
                }
            }
            default:
                return "";

        }
    }

    /**
     * Checks options params
     *
     * @param trade - OPTION
     * @return error text
     */
    private String checkOptions(Trade trade) {
        String errorText = "";

        if (!SUPPORTED_STYLES.contains(trade.getStyle())) {
            errorText += "Unsupported style " + trade.getStyle() + "; ";
        } else if (AMERICAN_STYLE.equals(trade.getStyle())) {
            if (trade.getExcerciseStartDate().before(trade.getTradeDate())) {
                errorText += "ExcerciseStartDate before trade date; ";
            }
            if (trade.getExcerciseStartDate().after(trade.getExpiryDate())) {
                errorText += "ExcerciseStartDate after expiry date; ";
            }


        } else {
            if (trade.getExpiryDate().after(trade.getDeliveryDate())
                    || trade.getPremiumDate().after(trade.getDeliveryDate())) {
                errorText += "Expiry date and premium date shall be before delivery date; ";
            }
        }

        return errorText;
    }

    /**
     * Checks date fall on weekend
     *
     * @param date - trade date
     * @return error text
     */
    private String checkWeekend(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (cal.get((Calendar.DAY_OF_WEEK)) == Calendar.SATURDAY ||
                cal.get((Calendar.DAY_OF_WEEK)) == Calendar.SUNDAY) {
            return "Value date cannot fall on weekend; ";
        }
        return "";
    }


}
