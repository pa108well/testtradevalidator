package com.meritcapital;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TradeController {

    @Autowired
    private ValidationService service;

    /**
     * @param trades - json
     * @return List of trades, each trade contains errorText after fail validation
     */
    @PutMapping("/validate")
    public ResponseEntity<?> getValidationResults(@RequestBody List<Trade> trades) {
        if (trades == null || trades.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        for (Trade trade : trades) {
            service.validateCommonTradeConditions(trade);
        }
        return ResponseEntity.ok(trades);
    }

}