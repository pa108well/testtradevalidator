package com.meritcapital;

import java.math.BigDecimal;
import java.util.Date;

public class Trade {

    private String customer;
    private Date tradeDate;
    private Date valueDate;
    private Date excerciseStartDate;
    private Date premiumDate;
    private Date expiryDate;
    private Date deliveryDate;
    private String ccyPair;
    private String payCcy;
    private String premiumCcy;
    private String type;
    private String direction;
    private String amount1;
    private String amount2;
    private String legalEntity;
    private String trader;
    private String style;
    private BigDecimal price;
    private Double rate;
    private String errorText;


    public Trade() {
    }


    public String getCustomer() {
        return customer;
    }

    public Date getTradeDate() {
        return tradeDate;
    }

    public Date getValueDate() {
        return valueDate;
    }

    public String getCcyPair() {
        return ccyPair;
    }

    public String getType() {
        return type;
    }

    public String getDirection() {
        return direction;
    }

    public String getAmount1() {
        return amount1;
    }

    public String getAmount2() {
        return amount2;
    }

    public String getLegalEntity() {
        return legalEntity;
    }

    public String getTrader() {
        return trader;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Double getRate() {
        return rate;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    public Date getExcerciseStartDate() {
        return excerciseStartDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public String getStyle() {
        return style;
    }

    public Date getPremiumDate() {
        return premiumDate;
    }

    public String getPayCcy() {
        return payCcy;
    }

    public String getPremiumCcy() {
        return premiumCcy;
    }
}
